const GRID_SIZE = 500;
const POP_SIZE = 100;
const PRINT = 20;

let grid;
let genCount = 0;

let pop;

function setup () {
    createCanvas(GRID_SIZE, GRID_SIZE);

    grid = new Grid(GRID_SIZE);

    pop = new Population(POP_SIZE, grid);
}

function draw () {
    pop.evolve();

    if (genCount % PRINT == 0) {
        let fittest = pop.getFittest();
        clear();
        background(10);
        drawMap();
        drawLine(fittest.path);
        console.log('gen:', genCount, 'fitness:', fittest.getFitness());
    }

    genCount += 1;
}

function drawLine(path) {
    let positions = grid.getPositions(path);
    stroke('orange');
    for (let pos of positions) {
        point(pos.x, pos.y);
    }
}

function drawMap () {
    stroke(255);
    fill('green');
    ellipse(grid.start.x, grid.start.y, 8);
    stroke(255);
    fill('blue');
    ellipse(grid.finish.x, grid.finish.y, 8);
    stroke('red');
    for (let obs of grid.obstacleList) {
        point(obs.x, obs.y);
    }
}
