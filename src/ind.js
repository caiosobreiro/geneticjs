class Ind{

    path = [];
    grid;
    maxLen;

    constructor (length, grid){
        this.grid = grid
        this.maxLen = grid.size / 10
        let path = []
        while (path.length < length) {
            path.push({ dir: _.random(0, 3), dist: _.random(0, this.maxLen) });
        }
        this.path = this.appendMovesToGetToFinish(path);
    }

    getFitness(){
        let fitness = 0;
        for (let vect of this.path)
            fitness += vect.dist;

        if (this.grid.detectOutOfBounds(this.path))
            fitness += 500;

        fitness += this.grid.detectCollisions(this.path) * 500;

        return 1 / fitness;
    }

    movesToGetToFinish(path) {

        let finalPos = this.grid.getFinalPosition(path);
        let moves = [];

        let remainingX = this.grid.finish.x - finalPos.x;
        moves.push({
            dir: remainingX > 0 ? this.grid.direction.right : this.grid.direction.left,
            dist: remainingX > 0 ? remainingX : -remainingX
        });

        let remainingY = this.grid.finish.y - finalPos.y;
        moves.push({
            dir: remainingY > 0 ? this.grid.direction.down : this.grid.direction.up,
            dist: remainingY > 0 ? remainingY : -remainingY
        });

        return moves;
    }

    appendMovesToGetToFinish(path) {
        let newPath = path.slice(0, -2);
        let remainingMoves = this.movesToGetToFinish(newPath);
        newPath = newPath.concat(remainingMoves);
        return newPath;
    }

    mutate() {
        let newPath = _.clone(this.path);
        newPath[_.random(0, this.path.length - 1)] = {dir: _.random(0, 3), dist: _.random(0, this.maxLen)};
        this.path = this.appendMovesToGetToFinish(newPath);
    }

    crossover(ind2) {
        const sublistSelf = this.path.slice(0, this.path.length / 2) // first half of this.path
        const sublistInd = ind2.path.slice(0, ind2.path.length / 2) // second half of ind2
        const newPath = sublistSelf.concat(sublistInd);
        this.path = this.appendMovesToGetToFinish(newPath);
    }
}
