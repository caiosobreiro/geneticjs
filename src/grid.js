class Grid {

    grid = [];
    size;
    obstacles;

    obstacleList = [];

    start;
    finish;

    startFinishDist;

    direction = {
        'up': 0,
        'right': 1,
        'down': 2,
        'left': 3
    };

    constructor(size) {

        this.size = size;
        this.obstacles = size ** 2 * 0.01; // set obstacles to 1% of the area;
        this.start = {
            x: _.random(0, this.size - 1),
            y: _.random(0, this.size - 1)
        };

        this.finish = {
            x: _.random(0, this.size - 1),
            y: _.random(0, this.size - 1)
        };

        this.startFinishDist = this.distanceToFinish(this.start);

        for (let i = 0; i < this.size; i++) {
            let row = [];
            for (let j = 0; j < this.size; j++) {
                row.push(0);
            }
            this.grid.push(row);
        }

        // set start and finish as 2 and 3
        this.grid[this.start.y][this.start.x] = 2;
        this.grid[this.finish.y][this.finish.x] = 3;

        // set obstacles randomly on the remaining spaces (where value = 0)
        let obstacleCounter = 0;
        while (obstacleCounter < this.obstacles) {
            let x = _.random(0, this.size - 1);
            let y = _.random(0, this.size - 1);
            if (this.grid[x][y] == 0) {
                this.grid[x][y] = 1;
                obstacleCounter += 1;
                this.obstacleList.push({x, y});
            }
        }
    }

    move(currentPos, direction) {
        let newCoords = _.clone(currentPos);

        if (direction == this.direction.up) {
            newCoords.x = currentPos.x;
            newCoords.y = currentPos.y - 1;
        } else if (direction == this.direction.right) {
            newCoords.x = currentPos.x + 1;
            newCoords.y = currentPos.y;
        } else if (direction == this.direction.left) {
            newCoords.x = currentPos.x - 1
            newCoords.y = currentPos.y;
        } else { // DOWN
            newCoords.x = currentPos.x;
            newCoords.y = currentPos.y + 1;
        }

        return newCoords;
    }

    getPositions(path) {
        let currentPos = _.clone(this.start);
        let positions = [currentPos];
        for (let vect of path) {
            for (let i = 0; i < vect.dist; i++) {
                currentPos = this.move(currentPos, vect.dir);
                positions.push(currentPos);
            }
        }
        return positions;
    }

    detectCollisions(path) {
        let positions = this.getPositions(path);
        let collisionCount = 0;

        for (let pos of positions) {
            if (pos.x >= 0 && pos.x < this.size && pos.y < this.size && pos.y >= 0 && this.grid[pos.y][pos.x] == 1)
                collisionCount += 1;
        }
        return collisionCount;
    }

    detectOutOfBounds(path){
        let positions = this.getPositions(path);
        for (let pos of positions)
            if (pos.x < 0 || pos.x >= this.size || pos.y >= this.size || pos.y < 0)
                return true;
        return false;
    }

    getFinalPosition(path) {
        let positions = this.getPositions(path);
        return positions[positions.length - 1];
    }

    distanceToFinish (currentPos) {
        return Math.abs(this.finish.x - currentPos.x) + Math.abs(this.finish.y - currentPos.y);
    }

    print (path = []) {

        let grid = _.cloneDeep(this.grid); // "deep _.clone" grid

        let dictionary = {
            0: '\u2B1B',    // open path
            1: '\u2B1C',    // obstacle
            2: '\u{1F535}', // start
            3: '\u274C',    // end
            4: '\u{1F536}'  // path
        }

       console.log('\n--------------------');
       console.log(dictionary[0], ' Open space');
       console.log(dictionary[1], ' Obstacle');
       console.log(dictionary[2], ' Start');
       console.log(dictionary[3], ' Finish');
       console.log(dictionary[4], ' Path');
       console.log('--------------------\n');

        let positions = this.getPositions(path).slice(0, -1);

        for (let position of positions)
            if (position.y < this.size && position.y >= 0 && position.x < this.size && position.x >= 0)
                grid[position.y][position.x] = 4;

        for (let i = 0; i < this.size; i++) {
            let row = ''
            for (let j = 0; j < this.size; j++)
                row += dictionary[grid[i][j]];
            console.log(row);
        }
        console.log();
    }
}
