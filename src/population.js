class Population{

    MUTATION = 0.01;
    CROSSOVER = 0.01;
    IND_LEN = 10;

    pop = [];
    size;
    grid;

    constructor(size, grid){
        this.size = size;
        this.grid = grid;
        for (let i = 0; i < size; i++) {
            let newInd = new Ind(this.IND_LEN, grid);
            this.pop.push(newInd);
        }
    }

    evolve(){
        let newPop = [];
        newPop = newPop.concat(this.acceptReject(this.size - 1));

        newPop.forEach(ind => {

            // mutation
            if (_.random() < this.MUTATION)
                ind.mutate();
            
            // crossover
            if (_.random() < this.CROSSOVER)
                ind.crossover(_.sample(newPop));
        })
        
        // pass fittest to next gen
        let fittest = this.getFittest();
        newPop.push(fittest);
        this.pop = newPop;
    }

    getFittest() {
        let fitnesses = [];
        this.pop.forEach(ind => {
            fitnesses.push(ind.getFitness());
        });
        let fittest = this.pop[fitnesses.indexOf(Math.max(...fitnesses))];
        return fittest;
    }

    // get n new individuals with fitness-based probability
    acceptReject(n) {
        let selected = [];

        while (selected.length < n){
            let randomInd = _.sample(this.pop);
            let r = _.random();
            if (randomInd.getFitness() * 40 > r){
                let newInd = new Ind(this.IND_LEN, this.grid);
                newInd.path = randomInd.path;
                selected.push(newInd);
            }
        }
        return selected;
    }
}
